# Title

Choose a self-explaining title for your project.

Describe what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here.

## Requirements

List any mandatory tools, libraries, packages etc. that the reader needs to setup, install and etc.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, use the **Requirements** section above.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Deployment

Describe how the app gets deployed, what are the environments it gets deployed to, and any other useful bits of information that relates to deployment.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors

Who are the main project owners, contributors and etc. List them -and how to reach out to them- here.

## Remarks

Any other information that doesn't neatly fall under the categories above, but it's still useful and should be included, you can add here. Feel free to skip this if it doesn't apply.
